
package ventil
{
	
	/**
	 * fra.CalcVentilation
	 *
	 * @author Adrien
	 */
	public class CalcVentilation
	{
		///////////////////////
		// PROPERTIES
		///////////////////////
		
		private var _ventilation:Ventilation;
		private var _surfaceHabitable:Number;
		private var _surfaceTotale:Number
		
		///////////////////////
		// METHODS
		///////////////////////
		
		public function CalcVentilation(ventilation:Ventilation)
		{
			this._surfaceHabitable = 120;
			this._surfaceTotale = 300;
			this._ventilation = ventilation
		}
		
		public function calcul_Hvent():Number
		{
			var res:Number = NaN;
			
			res = 0.34 * calcul_Qvarep() * this._surfaceHabitable * (1 - calcul_RventilNet());
			
			return Number(res.toFixed(5));
		}
		
		public function calcul_Hperm():Number
		{
			var res:Number = NaN;
			
			res = 0.34 * calcul_Qvinf();
			
			return Number(res.toFixed(5));
		}
		
		public function calcul_Qvinf():Number
		{
			var res:Number = NaN;
			
			res = 0.0146 * calcul_Q4pa() * Math.pow((0.7 * 19 - 8.08), 0.667);
			
			return Number(res.toFixed(5));
		}
		
		public function calcul_Qvarep():Number
		{
			var res:Number = NaN;
			
			if (ventilation == null)
			{
				throw new Error();
			}
			
			switch (ventilation.typeVentilation)
			{
				case Ventilation.SANS: 
					res = 1.2;
					break;
				case Ventilation.GRILLE: 
					res = 2.145;
					break;
				case Ventilation.HYGROREGLABLE: 
					res = 1.2375;
					break;
				case Ventilation.SIMPLE_FLUX: 
					res = 1.65;
					break;
				case Ventilation.DOUBLE_FLUX: 
					res = 1.65;
					break;
			}
			
			return res;
		}
		
		public function calcul_Smea():Number
		{
			var res:Number = NaN;
			
			if (ventilation == null)
			{
				throw new Error();
			}
			
			switch (ventilation.typeVentilation)
			{
				case Ventilation.SANS: 
				case Ventilation.DOUBLE_FLUX: 
					res = 0;
					break;
				case Ventilation.HYGROREGLABLE: 
				case Ventilation.SIMPLE_FLUX: 
					res = 2;
					break;
				case Ventilation.GRILLE: 
					res = 4;
					break;
			}
			
			return res;
		}
		
		public function calcul_Q4pa():Number
		{
			var res:Number = NaN;
			
			var coef:Number = 1.7;
			
			res = coef * this._surfaceTotale + 0.45 * calcul_Smea() + this._surfaceHabitable;
			
			return res;
		}
		
		public function calcul_RventilNet():Number
		{
			var res:Number = NaN;
			
			if (ventilation == null)
			{
				throw new Error();
			}
			
			if (ventilation.typeVentilation == Ventilation.DOUBLE_FLUX)
				res = ventilation.R - 0.12;
			else
				res = 0;
			
			return res;
		}
		
		public function calcul_DR():Number
		{
			var res:Number = NaN;
			
			res = calcul_Hvent() + calcul_Hperm();
			
			return res;
		}
		
		///////////////////////
		// GETTERS/SETTERS
		///////////////////////
		
		public function get ventilation():Ventilation
		{
			return _ventilation;
		}
	}
}