
package ventil
{
	
	/**
	 *
	 * @author Adrien
	 */
	public class Ventilation
	{
		///////////////////////
		// PROPERTIES
		///////////////////////
		
		public static const SANS:String = "sans ventilation";
		public static const GRILLE:String = "grilles hautes et basses";
		public static const SIMPLE_FLUX:String = "simple flux";
		public static const HYGROREGLABLE:String = "hygroréglable";
		public static const DOUBLE_FLUX:String = "double flux";
		
		private var _typeVentilation:String;
		
		private var _R:Number;
		
		///////////////////////
		// METHODS
		///////////////////////
		
		public function Ventilation(type:String)
		{
			_typeVentilation = type;
		}
		
		///////////////////////
		// GETTERS/SETTERS
		///////////////////////
		
		public function get typeVentilation():String
		{
			return _typeVentilation;
		}
		
		public function get R():Number
		{
			return _R;
		}
		
		public function set R(value:Number):void
		{
			_R = value;
		}
	}
}