package 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import mockolate.prepare;
	import ventil.Ventilation;
	
	/**
	 * ...
	 * @author Adrien
	 */
	public class PrepareMockolate extends EventDispatcher 
	{
		public function PrepareMockolate() 
		{
		}
		
		public function prepareMockolate():void
		{
			var t:Timer = new Timer(1000, 1);
			t.addEventListener(TimerEvent.TIMER_COMPLETE, onTimer);
			t.start();
			
			prepare(Ventilation);
		}
		
		private function onTimer(evt:TimerEvent):void
		{
			(evt.target as Timer).stop();
			
			dispatchEvent(new Event(Event.COMPLETE));
		}
	}
	
}