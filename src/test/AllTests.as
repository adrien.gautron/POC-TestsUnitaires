package test
{
	import asunit.framework.TestSuite;
	
	/**
	 * ...
	 * @author Adrien
	 */
	public class AllTests extends TestSuite 
	{
		public function AllTests()
		{
			super();
			addTest(new TestCalcVentilation("test_calcul_Smea"));
			addTest(new TestCalcVentilation("test_calcul_Smea_error"));
		}
	}
	
}