package test_old
{
	
	/**
	 * Contient le fonction de test unitaire
	 * @author Adrien
	 */
	public class TestUnitaire_old
	{
		public function TestUnitaire_old()
		{
		
		}
		
		/**
		 * Teste une fonction.
		 * Fait une trace du resultat et retourne le resultat
		 * 
		 * @param Function	fonction	fonction à tester
		 * @param *			expectedReturn retour attendu de la fonction
		 * @return testOk
		 */
		public function testUnitaireFonction(fonction:Function, expectedReturn:*):Boolean
		{
			var testOk:Boolean = false;
			
			var effectiveReturn:*;
			
			if (expectedReturn is Error)
			{
				try
				{
					fonction();
				}
				catch (err:Error)
				{
					testOk = true;
				}
			}
			else if (fonction() == expectedReturn)
			{
				testOk = true;
			}
			else if (isNaN(expectedReturn) && isNaN(fonction()))
			{
				testOk = true;
			}
			else
			{
				effectiveReturn = fonction();
			}
			
			if (testOk)
			{
				trace("ok");
			}
			else
			{
				trace("erreur - resultat attendu : " + expectedReturn + " - resultat obtenu : " + effectiveReturn);
			}
			
			return testOk;
		}
	}

}