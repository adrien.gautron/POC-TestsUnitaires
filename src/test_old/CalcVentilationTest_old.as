package test_old
{
	import flash.events.EventDispatcher;
	import ventil.CalcVentilation;
	import ventil.Ventilation;
	
	/**
	 * ...
	 * @author Adrien
	 */
	public class CalcVentilationTest_old extends EventDispatcher
	{
		private var _calcVentilation:ventil.CalcVentilation;
		private var _test:TestUnitaire_old;
		
		public function CalcVentilationTest_old()
		{
			this._test = new TestUnitaire_old();
		}
		
		/**
		 * Test de toute les fonctions de CalcVentilation.
		 * Utilisation de différentes valeurs de ventilation:Ventilation.
		 * 
		 * Retourne le resultat de l'integralité des tests sur la ventilation
		 * 
		 * @return testResult
		 */
		public function testAll():Boolean
		{
			var testResult:Boolean = true;
			
			// ventilation null
			var ventilation:Ventilation;
			this._calcVentilation = new CalcVentilation(ventilation);
			
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Smea, new Error()) && testResult;
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Q4pa, new Error()) && testResult;
			
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Qvarep, new Error()) && testResult;
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Hvent, new Error()) && testResult;
			
			// type de ventilation inconnu
			ventilation = new Ventilation("abc");
			this._calcVentilation = new CalcVentilation(ventilation);
			
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Smea, NaN) && testResult;
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Q4pa, NaN) && testResult;
			
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Qvarep, NaN) && testResult;
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Hvent, NaN) && testResult;
			
			// ventilation hygro-reglable connue
			ventilation = new Ventilation(Ventilation.HYGROREGLABLE);
			this._calcVentilation = new CalcVentilation(ventilation);
			
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Smea, 2) && testResult;
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Q4pa, 630.9) && testResult;
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Qvinf, 27.73318) && testResult;
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Hperm, 9.42928) && testResult;
			
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Qvarep, 1.2375) && testResult;
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_RventilNet, 0) && testResult;
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Hvent, 50.49) && testResult;
			
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_DR, 59.91928) && testResult;
			
			// ventilation double flux connue
			ventilation = new Ventilation(Ventilation.DOUBLE_FLUX);
			ventilation.R = 0.85;
			this._calcVentilation = new CalcVentilation(ventilation);
			
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Smea, 0) && testResult;
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Q4pa, 630) && testResult;
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Qvinf, 30) && testResult; // valeur attendue volontairement fausse, Hperm et DR en découlent
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Hperm, 10.2) && testResult;
			
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Qvarep, 1.65) && testResult;
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_RventilNet, 0.73) && testResult;
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_Hvent, 18.1764) && testResult;
			
			testResult = this._test.testUnitaireFonction(this._calcVentilation.calcul_DR, 28.3764) && testResult;
			
			return testResult;
		}
	}

}