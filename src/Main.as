package
{
	import asunit.textui.TestRunner;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import mockolate.prepare;
	import test.AllTests;
	import ventil.Ventilation;
	
	/**
	 * ...
	 * @author Adrien
	 */
	public class Main extends Sprite
	{
		public function Main()
		{
			var preparation:PrepareMockolate = new PrepareMockolate();
			preparation.addEventListener(Event.COMPLETE, onPrepareComplete);
			preparation.prepareMockolate();
			
			////////////////////
			// TEST_OLD
			////////////////////
			/*var texte:String;
			
			if (new CalcVentilationTest().testAll())
			{
				texte = "Tests OK"
			}
			else
			{
				texte = "Tests KO";
			}
			
			var f:TextFormat = new TextFormat("normal", 30, 0x000000, true);
			var tf:TextField = new TextField();
			
			tf.text = texte;
			
			tf.setTextFormat(f);
			tf.autoSize = TextFieldAutoSize.CENTER;
			tf.x = (800 - tf.width) / 2;
			tf.y = (600 - tf.height) / 2;
			this.addChild(tf);*/
		}
		
		private function onPrepareComplete(evt:Event):void
		{
			var unittests:TestRunner = new TestRunner();
			stage.addChild(unittests);
			unittests.start(AllTests, null, TestRunner.SHOW_TRACE);
		}
	}

}