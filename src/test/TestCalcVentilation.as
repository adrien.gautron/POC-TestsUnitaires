package test
{
	import asunit.framework.TestCase;
	import mockolate.mock;
	import mockolate.nice;
	import ventil.CalcVentilation;
	import ventil.Ventilation;
	
	/**
	 * ...
	 * @author Adrien
	 */
	public class TestCalcVentilation extends TestCase
	{
		public function TestCalcVentilation(testMethod:String)
		{
			super(testMethod);
		}
		
		public function test_calcul_Smea():void
		{
			var ventilation:Ventilation = nice(Ventilation);
			mock(ventilation).getter("typeVentilation").returns(Ventilation.DOUBLE_FLUX);
			mock(ventilation).getter("R").returns(0.75);
			
			var calcVentilation:CalcVentilation = new CalcVentilation(ventilation);
			
			assertEquals(calcVentilation.calcul_Smea(), 0);
		}
		
		public function test_calcul_Smea_error():void
		{
			var calcVentilation:CalcVentilation = new CalcVentilation(null);
			assertThrows(Error, calcVentilation.calcul_Smea);
		}
		
	}
	
}